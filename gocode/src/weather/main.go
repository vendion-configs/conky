// Copyright © 2016 Adam Jimerson <vendion@gmail.com>
// This work is free. You can redistribute it and/or modify it under the
// terms of the Do What The Fuck You Want To Public License, Version 2,
// as published by Sam Hocevar. See the COPYING file for more details.

package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	owm "github.com/briandowns/openweathermap"
)

var debug bool

const currentWeatherFile = "%s/.cache/currentweather.json"
const weatherIcons = "%s/.config/conky/conky-google-now/%s.png"

func init() {
	flag.BoolVar(&debug, `d`, false, `Run in verbose debug mode, displays additional information while running`)
}

func main() {
	flag.Parse()
	w := getCurrentWeather()
	if debug {
		log.Println(w)
	}
	formatWeather(w)
}

// writeWeatherCacheFile dates data from OpenWeatherMap formats it as JSON then
// writes the JSON to a file so it could be read from later.
func writeWeatherCacheFile(w *owm.CurrentWeatherData) {
	w.CurrentByID(4612862)
	b, err := json.Marshal(w)
	if err != nil {
		log.Println(err)
	}

	fi, err := os.Create(fmt.Sprintf(currentWeatherFile, os.Getenv("HOME")))
	if err != nil {
		log.Fatalln(err)
	}
	defer fi.Close()

	writer := bufio.NewWriter(fi)
	fmt.Fprintln(writer, string(b))
	writer.Flush()
}

// getCurrentWeather checks if there is local weather data cached and is current,
// if so then it returns that data, else it fetches newer data from OpenWeatherMap
// API and triggers cache updating.
func getCurrentWeather() *owm.CurrentWeatherData {
	var fileExists bool
	// check if there is already cached weather data
	fi, err := os.Stat(fmt.Sprintf(currentWeatherFile, os.Getenv("HOME")))
	if err != nil {
		// check if the error is becase there is no cache file (local cache dir purged or first run)
		if os.IsNotExist(err) {
			// file does not exist
			fileExists = false
		} else {
			// error is because of other reasons
			log.Fatalln(err)
		}
	}
	fileExists = true

	t := time.Now()
	if fileExists == true {
		// check if the cache file is less than 3 hours old
		if t.Before(fi.ModTime()) {
			if debug {
				log.Println("possible time travel detected, file modtime is after current time!")
			}
			// file time in the future, file system time travel occured?
			// treating as if file is not present
			fileExists = false
		}
	}

	if fileExists == true {
		d := t.Sub(fi.ModTime())
		if d.Hours() >= 3 {
			if debug {
				log.Println("cache file is older than 3 hours, refreshing...")
			}
			// file too old treating as not present
			fileExists = false
		}
	}

	// get weather data
	var w *owm.CurrentWeatherData
	if fileExists {
		// read data from cache file
		cacheFile, err := os.Open(fmt.Sprintf(currentWeatherFile, os.Getenv("HOME")))
		if err != nil {
			if debug {
				log.Println(err)
			}
			w, err = owm.NewCurrent("F", "en")
			if err != nil {
				log.Fatalln(err)
			}
			return w
		}
		defer cacheFile.Close()

		// if `w` is already populated then we had to fetch from OpenWeatherMap due to file error,
		// so don't try to read from the filea
		s := bufio.NewScanner(cacheFile)
		for s.Scan() {
			data := s.Bytes()
			if err = json.Unmarshal(data, &w); err != nil {
				log.Fatalln(err)
			}
		}
		if err := s.Err(); err != nil {
			log.Fatalln(err)
		}
	} else {
		// pull data from OpenWeatherMap and write to cache
		w, err = owm.NewCurrent("F", "en")
		if err != nil {
			log.Fatalln(err)
		}
		writeWeatherCacheFile(w)
	}

	return w
}

// formatWeather formats the weather data for use in Conky.
func formatWeather(w *owm.CurrentWeatherData) {
	fmt.Print("${font Roboto Light:size=15}" + w.Name + "${font}\n")
	fmt.Println("${font Roboto Light:size=20}${alignr}" + w.Weather[0].Main + "${font}${voffset -40}")
	icon := weatherIcon(w.Weather[0].Icon)
	fmt.Println("${image " + icon + " -p 0,45 -s 60x60}")
	fmt.Println("${voffset 20}${alignr}" + w.Weather[0].Description)
	fmt.Printf("${alignr}${font Roboto Light:size=30} %2.1f ${font Roboto Light:size=12}(%2.1f/%2.1f)${font}\n",
		w.Main.Temp,
		w.Main.TempMin,
		w.Main.TempMax)
	icon = weatherIcon("wind")
	fmt.Printf("${image %v -p 0,110 -s 15x15}${goto 35} %3.1f MPH %3.1f\n", icon, w.Wind.Speed, w.Wind.Deg)
	if w.Rain["3h"] > 0.0 || w.Snow["3h"] > 0.0 {
		icon = weatherIcon("humidity")
		fmt.Printf("${image %v -p 0,130 -s 15x15}${goto 35}", icon)
		if w.Rain["3h"] > 0.0 {
			fmt.Printf("%3.1f%% (rain)", w.Rain["3h"])
		}
		if w.Snow["3h"] > 0.0 {
			fmt.Printf("%3.1f%% (snow)", w.Snow["3h"])
		}
		fmt.Println()
	}
	fmt.Printf("Cloud coverage %d%%\n", w.Clouds.All)
	sunrise := time.Unix(int64(w.Sys.Sunrise), 0)
	sunset := time.Unix(int64(w.Sys.Sunset), 0)
	fmt.Println("Sunrise " + sunrise.Local().Format(time.Kitchen) + " Sunset " + sunset.Local().Format(time.Kitchen))
}

// weatherIcon takes a code from OpenWeatherMap and returns the path to an image
// to use to represent the current weather condition.
func weatherIcon(code string) string {
	if code == "" {
		code = "3200"
	}
	return fmt.Sprintf(weatherIcons, os.Getenv("HOME"), code)
}
