# Weather

This is a utility designed to work with Conky and OpenWeatherMap API.  Weather
fetches weather information from OpenWeatherMap and caches it, when ran it will
either return the cached data or download current weather information depending
on how old the cache version is.

## Installing

While this is go getable, it really isn't intended to be used out side of this
Conky repo.  Weather depends on Go 1.5 with the `GO15VENDOREXPERIMENT` environment
variable enabled, or 1.6+.

### Building

Weather can be built one of two ways once it has been downloaded:

  go build .
  mv weather ${HOME}/bin/

or by

  go install .

Which will place the binary in `$GOPATH/bin` which should already be in the
users `$PATH` as per the Go standard.

## License

Copyright © 2016 Adam Jimerson <vendion@gmail.com>
This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the COPYING file for more details.
